package com.example.task1.common

class AppError(title: String, code: Int, message: String) : Exception(message) {

    constructor(title: String, code: Int) : this(title, code, "")

    constructor(title: String) : this(title, 500)
}