package com.example.task1.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.task1.common.AppError
import com.example.task1.data.model.UserData
import com.example.task1.data.repository.source.UserRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthenticationViewModel @Inject constructor(
    private val userRepository: UserRepositoryImpl
) : ViewModel() {

    private var _loginResult = MutableLiveData<Boolean>()
    val loginResult: LiveData<Boolean> = _loginResult

    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    fun loginClick() {
        login(username.value!!, password.value!!)
    }

    private fun login(username: String, password: String) {

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val requestToken = userRepository.getRequestToken().request_token
                Log.e("KKKKKKKKKKKKK", requestToken)
                val validateLogin = userRepository.validateWithLogin(
                    username,
                    password,
                    requestToken
                )
/* val validateLogin = userRepository.validateWithLogin(
                    "SaharN",
                    "task1234",
                    "c45671b27dd26ba8d0040b14729b8439ad2ec367"
                )
*/
                _loginResult.postValue(validateLogin.success)

                userRepository.saveLogin(validateLogin.success)
                userRepository.saveUserData(
                    UserData(
                        username,
                        password,
                        requestToken
                    )
                )
            } catch (e: AppError) {
                _errorMessage.postValue(e.message)
            }
        }
    }

    fun checkLogin() = userRepository.checkLogin()
}