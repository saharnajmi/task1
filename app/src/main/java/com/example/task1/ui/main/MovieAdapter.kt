package com.example.task1.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.task1.data.model.Movie
import com.example.task1.databinding.ItemMoiveBinding
import java.util.*


class MovieAdapter(
    private var movie: ArrayList<Movie.Result>,
    private val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>(), Filterable {

    var filterList = ArrayList<Movie.Result>()

    fun addData(list: ArrayList<Movie.Result>) {
        filterList = list
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemMoiveBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Movie.Result) {
            binding.movieModel = item
            itemView.setOnClickListener {
                onItemClick.onClick(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemMoiveBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(movie[position])

    override fun getItemCount(): Int = movie.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults? {
                val filterResult = FilterResults()
                if (charSequence == null) {
                    filterList = movie
                } else {
                    val searChar = charSequence.toString().lowercase(Locale.ROOT)
                    val itemModel = ArrayList<Movie.Result>()
                    for (item in filterList) {
                        if (item.title.lowercase(Locale.ROOT).contains(searChar)) {
                            itemModel.add(item)
                        }
                    }
                    filterResult.count = itemModel.size
                    filterResult.values = itemModel
                }
                return filterResult
            }

            override fun publishResults(constraint: CharSequence?, filterResults: FilterResults?) {
                movie = filterResults?.values as ArrayList<Movie.Result>
                notifyDataSetChanged()
            }
        }
    }

    interface OnItemClick {
        fun onClick(movie: Movie.Result)
    }
}