package com.example.task1.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.task1.R
import com.example.task1.common.showMessage
import com.example.task1.databinding.ActivityAuthenticationBinding
import com.example.task1.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthenticationActivity : AppCompatActivity() {
    lateinit var binding: ActivityAuthenticationBinding
    private val viewModel: AuthenticationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_authentication)

        binding.authViewModel = viewModel

        if (viewModel.checkLogin()) {
            startActivity(Intent(this, MainActivity::class.java))
        }

        viewModel.errorMessage.observe(this) {
            showMessage(it)
        }

        viewModel.loginResult.observe(this) { status ->
            if (status) {
                showMessage("ورود با موفقیت انجام شد")

                startActivity(Intent(this, MainActivity::class.java))
            } else {
                showMessage("ورود با شکست مواجه شد")
            }
        }
    }
}