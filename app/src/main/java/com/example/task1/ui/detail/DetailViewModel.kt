package com.example.task1.ui.detail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1.data.model.Generes
import com.example.task1.data.repository.source.GenreRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val genreRepository: GenreRepositoryImpl
) : ViewModel() {
    private var genres = MutableLiveData<List<Generes.Genre>>()

    init {
        getLocalGenres()
    }

    private fun getLocalGenres() {
        if (genreRepository.getLocaleGenres().isEmpty())
            getRemoteGenres()
        else
            genres.value = genreRepository.getLocaleGenres()
    }

    private fun getRemoteGenres() {
        genreRepository.getRemoteGenres().enqueue(object : Callback<Generes> {
            override fun onResponse(call: Call<Generes>, response: Response<Generes>) {
                if (response.isSuccessful) {
                    genres.value = response.body()!!.genres
                    genreRepository.saveGenres(response.body()!!.genres)
                }
            }

            override fun onFailure(call: Call<Generes>, t: Throwable) {
                Log.e("LOG ERROR", t.toString())
            }
        })
    }

    fun getGenresName(genreIds: List<Int>): List<Generes.Genre> {
        val genreNames = ArrayList<Generes.Genre>()
        genreIds.forEach { ids ->
            for (i in 0 until genres.value!!.size) {
                if (ids == genres.value!![i].id) {
                    genreNames.add(genres.value!![i])
                }
            }
        }
        return genreNames
    }
}