package com.example.task1.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task1.R
import com.example.task1.common.showMessage
import com.example.task1.data.model.Movie
import com.example.task1.databinding.ActivityMainBinding
import com.example.task1.ui.detail.DetailActivity
import com.example.task1.ui.login.AuthenticationActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), MovieAdapter.OnItemClick {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    lateinit var movieAdapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.mainViewModel = viewModel

        viewModel.errorMessage.observe(this) {
            showMessage("مشکل در ارتباط با سرور")
        }

        viewModel.movies.observe(this) { movies ->
            binding.recyclerView.layoutManager =
                GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
            movieAdapter = MovieAdapter(movies as ArrayList<Movie.Result>, this)
            binding.recyclerView.adapter = movieAdapter

            movieAdapter.addData(movies)
        }

        //refresh button
        binding.refresh.setOnClickListener {
            viewModel.deleteMovies()
        }

        binding.logOut.setOnClickListener {
            startActivity(Intent(this, AuthenticationActivity::class.java))
            viewModel.logOut()
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                movieAdapter.filter.filter(newText)
                return true
            }
        })
    }

    override fun onClick(movie: Movie.Result) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("movie", movie)
        startActivity(intent)
    }
}