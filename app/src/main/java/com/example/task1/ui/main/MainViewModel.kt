package com.example.task1.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1.data.model.Movie
import com.example.task1.data.repository.source.MovieRepositoryImpl
import com.example.task1.data.repository.source.UserRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val movieRepository: MovieRepositoryImpl,
    private val userRepository: UserRepositoryImpl
) : ViewModel() {

    private var _movies = MutableLiveData<List<Movie.Result>>()
    val movies: LiveData<List<Movie.Result>> = _movies

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    val errorMessage = MutableLiveData<String>()

    val username = MutableLiveData<String>()

    init {
        getLocalMovies()
        username.value = userRepository.getUserData().userName
    }

    private fun getLocalMovies() {
        if (movieRepository.getLocaleMovies().isEmpty())
            getRemoteMovies()
        else
            _movies.value = movieRepository.getLocaleMovies()
    }

    private fun getRemoteMovies() {
        movieRepository.getRemoteMovies().enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                if (response.isSuccessful) {
                    _movies.value = response.body()!!.results
                    _loading.value = false
                    movieRepository.saveMovies(response.body()!!.results)
                } else {
                    _loading.value = true
                }
            }

            override fun onFailure(call: Call<Movie>, t: Throwable) {
                Log.e("LOG ERROR", t.toString())
                _loading.value = true
                errorMessage.postValue(t.message)
            }
        })
    }


    fun deleteMovies() {
        movieRepository.deleteMovies()
        getRemoteMovies()
    }

    fun logOut() = userRepository.logOut()

}