package com.example.task1.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task1.data.model.Generes
import com.example.task1.databinding.ItemGenreBinding

class GenreAdapter(private val genres: List<Generes.Genre>) :
    RecyclerView.Adapter<GenreAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemGenreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Generes.Genre) {
            binding.genreModel = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(genres[position])

    override fun getItemCount(): Int = genres.size
}