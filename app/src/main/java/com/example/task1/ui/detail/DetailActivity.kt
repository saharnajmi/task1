package com.example.task1.ui.detail

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task1.R
import com.example.task1.data.model.Movie
import com.example.task1.databinding.ActivityDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.lifecycleOwner = this
        val movieDetail = intent.extras!!.getParcelable<Movie.Result>("movie")

        binding.detailModel = viewModel
        binding.movieModel = movieDetail

        binding.recyclerViewGenres.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding.recyclerViewGenres.adapter =
            GenreAdapter(viewModel.getGenresName(movieDetail!!.genre_ids))

        Log.e("genreNames", viewModel.getGenresName(movieDetail.genre_ids).toString())
    }

}