package com.example.task1.network.okhttp

import com.example.task1.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request

class OkHttpInterceptor() {
    fun apiKeyInterceptor(): Interceptor = Interceptor { chain ->
        val request: Request = chain.request()
        val url = request.url().newBuilder()
            .addQueryParameter("api_key", BuildConfig.API_KEY)
            .build()
        val newRequest = request.newBuilder()
            .url(url).build()
        chain.proceed(newRequest)
    }
}