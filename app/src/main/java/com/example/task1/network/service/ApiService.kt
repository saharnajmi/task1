package com.example.task1.network.service

import com.example.task1.data.model.Generes
import com.example.task1.data.model.Movie
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("genre/movie/list?language=en-US")
    fun getGenres(): Call<Generes>

    /*@GET("authentication/token/new")
    fun getRequestToken(): Call<RequestToken>

    @FormUrlEncoded
    @POST("authentication/token/validate_with_login")
    fun validateWithLogin(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("request_token") request_token: String
    ): Call<RequestToken>*/

    @GET("movie/popular")
    fun getPopularMovies(): Call<Movie>
}