package com.example.task1.network.okhttp

import com.example.task1.data.repository.source.UserRepository
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class RequestInterceptor @Inject constructor(private val userRepository: UserRepository) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        if (chain.request().url().toString().contains("/token/new")) {
            builder.addHeader(
                "Authorization",
                "Bearer ${userRepository.getRequestToken()}"
            ).build()
        }
        return chain.proceed(builder.build())
    }
}