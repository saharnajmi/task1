package com.example.task1.network.service

import com.example.task1.data.model.RequestToken
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthService {
    @GET("authentication/token/new")
    fun getRequestToken(): Call<RequestToken>

    @FormUrlEncoded
    @POST("authentication/token/validate_with_login")
    fun validateWithLogin(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("request_token") request_token: String
    ): Call<RequestToken>
}