package com.example.task1.network.okhttp

import com.example.task1.data.model.UserData
import com.example.task1.data.repository.source.UserRepository
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    private val userRepository: UserRepository
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val updatedToken = getUpdatedToken()
        return response.request().newBuilder()
            .header("Authorization", updatedToken)
            .build()
    }

    private fun getUpdatedToken(): String {
        val authTokenResponse = userRepository.getRequestToken()

        val newToken = authTokenResponse.request_token

        userRepository.saveUserData(
            UserData(
                userRepository.getUserData().userName,
                userRepository.getUserData().password,
                requestToken = newToken
            )
        )
        return newToken
    }
}