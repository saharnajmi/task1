package com.example.task1.di.module

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.room.Room
import com.example.task1.BuildConfig
import com.example.task1.data.db.AppDataBase
import com.example.task1.data.repository.source.GenreRepositoryImpl
import com.example.task1.data.repository.source.UserRepository
import com.example.task1.data.repository.source.UserRepositoryImpl
import com.example.task1.data.repository.source.local.GenreLocalDataSource
import com.example.task1.data.repository.source.local.MovieLocalDataSource
import com.example.task1.data.repository.source.local.UserLocalDataSource
import com.example.task1.data.repository.source.local.UserLocalDataSourceImpl
import com.example.task1.data.repository.source.remote.*
import com.example.task1.network.okhttp.LoginInterceptor
import com.example.task1.network.okhttp.OkHttpInterceptor
import com.example.task1.network.okhttp.RequestInterceptor
import com.example.task1.network.okhttp.TokenAuthenticator
import com.example.task1.network.service.ApiService
import com.example.task1.network.service.AuthService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideApiKeyInterceptor(): Interceptor = OkHttpInterceptor().apiKeyInterceptor()

    /* @Singleton
     @Provides
     fun loginInterceptor(): Interceptor = LoginInterceptor()*/

    @Singleton
    @Provides
    fun provideOkHttpClient(
        apiKeyInterceptor: Interceptor,
        requestInterceptor: RequestInterceptor,
        tokenAuthenticator: TokenAuthenticator
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .addInterceptor(requestInterceptor)
            .addInterceptor(LoginInterceptor())
            .authenticator(tokenAuthenticator)
            .build()

    @Singleton
    @Provides
    fun provideTokenAuthenticator(userRepository: UserRepository): TokenAuthenticator =
        TokenAuthenticator(userRepository)

    @Singleton
    @Provides
    fun provideRequestInterceptor(userRepository: UserRepository): RequestInterceptor =
        RequestInterceptor(userRepository)

    @Singleton
    @Provides
    fun provideAuthRetrofitInstance(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()

    @Singleton
    @Provides
    fun provideAuthRetrofitService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)

    @Provides
    fun provideRetrofitService(
        apiKeyInterceptor: Interceptor
    ): AuthService =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(apiKeyInterceptor)
                    .addInterceptor(LoginInterceptor())
                    .build()
            )
            .build()
            .create(AuthService::class.java)

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        context.getSharedPreferences("shared_pref", MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideAppDataBase(@ApplicationContext context: Context): AppDataBase =
        Room.databaseBuilder(context, AppDataBase::class.java, "movie_database")
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun provideGenreRemoteDataSource(apiService: ApiService): GenreRemoteDataSource =
        GenreRemoteDataSourceImpl(apiService)

    @Singleton
    @Provides
    fun provideMovieRemoteDataSource(apiService: ApiService): MovieRemoteDataSource =
        MovieRemoteDataSourceImpl(apiService)

    @Singleton
    @Provides
    fun provideMovieLocalDataSource(appDataBase: AppDataBase): MovieLocalDataSource =
        appDataBase.movieDao()

    @Singleton
    @Provides
    fun provideGenreLocalDataSource(appDataBase: AppDataBase): GenreLocalDataSource =
        appDataBase.genreDao()

    @Singleton
    @Provides
    fun provideGenreRepository(
        genreLocalDataSource: GenreLocalDataSource,
        genreRemoteDataSource: GenreRemoteDataSource
    ): GenreRepositoryImpl =
        GenreRepositoryImpl(
            genreLocalDataSource,
            genreRemoteDataSource
        )

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(authService: AuthService): UserRemoteDataSource =
        UserRemoteDataSourceImpl(authService)

    @Singleton
    @Provides
    fun provideUserLocalDataSource(sharedPreferences: SharedPreferences): UserLocalDataSource =
        UserLocalDataSourceImpl(sharedPreferences)

    @Singleton
    @Provides
    fun provideUserRepository(
        userRemoteDataSource: UserRemoteDataSource,
        userLocalDataSource: UserLocalDataSource
    ): UserRepository = UserRepositoryImpl(
        userRemoteDataSource, userLocalDataSource
    )
}