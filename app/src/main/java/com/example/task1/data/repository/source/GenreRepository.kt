package com.example.task1.data.repository.source

import com.example.task1.data.model.Generes
import retrofit2.Call

interface GenreRepository {
    fun getRemoteGenres(): Call<Generes>

    fun getLocaleGenres(): List<Generes.Genre>

    fun saveGenres(movies: List<Generes.Genre>)

}