package com.example.task1.data.repository.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.task1.data.model.Movie

@Dao
interface MovieLocalDataSourceImpl : MovieLocalDataSource {

    @Query("SELECT * FROM movie")
    override fun getMovies(): List<Movie.Result>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun saveMovies(movies: List<Movie.Result>)

    @Query("DELETE FROM movie")
    override fun deleteMovies()

}