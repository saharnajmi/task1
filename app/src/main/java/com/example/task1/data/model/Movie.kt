package com.example.task1.data.model

import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.bumptech.glide.Glide
import kotlinx.parcelize.Parcelize

data class Movie(
    val page: Int,
    val results: List<Result>,
    val total_pages: Int,
    val total_results: Int
) {
    @Entity(tableName = "movie")
    @Parcelize
    data class Result(
        @PrimaryKey(autoGenerate = false)
        val id: Int,
        val adult: Boolean,
        val backdrop_path: String,
        @TypeConverters(IntTypeConverter::class)
        val genre_ids: List<Int>,
        val original_language: String,
        val original_title: String,
        val overview: String,
        val popularity: Double,
        val poster_path: String,
        val release_date: String,
        val title: String,
        val video: Boolean,
        val vote_average: Double,
        val vote_count: Int
    ) : Parcelable

    companion object {
        @BindingAdapter("android:loadImage")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String) {
            Glide.with(view.context)
                .load("https://image.tmdb.org/t/p/w300_and_h450_bestv2/$imageUrl")
                .into(view)
        }
    }
}