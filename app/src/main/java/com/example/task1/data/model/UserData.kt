package com.example.task1.data.model

data class UserData(
    val userName: String,
    val password: String,
    val requestToken: String
)
