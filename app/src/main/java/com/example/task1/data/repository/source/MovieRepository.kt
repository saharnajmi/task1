package com.example.task1.data.repository.source

import com.example.task1.data.model.Movie
import retrofit2.Call

interface MovieRepository {
    fun getRemoteMovies(): Call<Movie>

    fun getLocaleMovies(): List<Movie.Result>

    fun saveMovies(movies: List<Movie.Result>)

    fun deleteMovies()
}