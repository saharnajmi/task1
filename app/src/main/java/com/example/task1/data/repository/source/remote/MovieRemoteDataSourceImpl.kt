package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.Movie
import com.example.task1.network.service.ApiService
import retrofit2.Call
import javax.inject.Inject

class MovieRemoteDataSourceImpl @Inject constructor(private val apiService: ApiService) :
    MovieRemoteDataSource {

    override fun getMovies(): Call<Movie> = apiService.getPopularMovies()
}