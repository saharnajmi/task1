package com.example.task1.data.repository.source.local

import com.example.task1.data.model.Generes

interface GenreLocalDataSource {
    fun getGenres(): List<Generes.Genre>

    fun saveGenres(genres: List<Generes.Genre>)
}