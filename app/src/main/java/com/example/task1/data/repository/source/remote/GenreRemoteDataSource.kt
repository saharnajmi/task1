package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.Generes
import retrofit2.Call

interface GenreRemoteDataSource {
    fun getGenres(): Call<Generes>
}