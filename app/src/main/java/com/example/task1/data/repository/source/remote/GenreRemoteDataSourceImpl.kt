package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.Generes
import com.example.task1.network.service.ApiService
import retrofit2.Call

class GenreRemoteDataSourceImpl(private val apiService: ApiService) : GenreRemoteDataSource {
    override fun getGenres(): Call<Generes> = apiService.getGenres()
}