package com.example.task1.data.repository.source

import com.example.task1.data.model.Movie
import com.example.task1.data.repository.source.local.MovieLocalDataSource
import com.example.task1.data.repository.source.remote.MovieRemoteDataSource
import retrofit2.Call
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    private val movieLocalDataSource: MovieLocalDataSource
) : MovieRepository {

    override fun getRemoteMovies(): Call<Movie> =
        movieRemoteDataSource.getMovies()

    override fun getLocaleMovies(): List<Movie.Result> =
        movieLocalDataSource.getMovies()

    override fun saveMovies(movies: List<Movie.Result>) =
        movieLocalDataSource.saveMovies(movies)

    override fun deleteMovies() =
        movieLocalDataSource.deleteMovies()
}