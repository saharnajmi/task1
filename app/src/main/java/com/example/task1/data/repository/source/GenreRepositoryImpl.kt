package com.example.task1.data.repository.source

import com.example.task1.data.model.Generes
import com.example.task1.data.repository.source.local.GenreLocalDataSource
import com.example.task1.data.repository.source.remote.GenreRemoteDataSource
import retrofit2.Call
import javax.inject.Inject

class GenreRepositoryImpl @Inject constructor(
    private val genreLocalDataSource: GenreLocalDataSource,
    private val genreRemoteDataSource: GenreRemoteDataSource
) : GenreRepository {
    override fun getRemoteGenres(): Call<Generes> =
        genreRemoteDataSource.getGenres()

    override fun getLocaleGenres(): List<Generes.Genre> =
        genreLocalDataSource.getGenres()

    override fun saveGenres(genres: List<Generes.Genre>) =
        genreLocalDataSource.saveGenres(genres)
}