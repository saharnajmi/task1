package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.RequestToken
import retrofit2.Call

interface UserRemoteDataSource {
    fun getRequestToken(): Call<RequestToken>

    fun validateWithLogin(
        username: String,
        password: String,
        requestToken: String
    ): Call<RequestToken>
}