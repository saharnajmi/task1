package com.example.task1.data.model

data class RequestToken(
    val expires_at: String,
    val request_token: String,
    val success: Boolean
)

/*
data class RequestToken(
    val success: Boolean,
    @Expose
    val expires_at: String,
    @Expose
    val request_token: String,
    @Expose
    val status_code: Int,
    @Expose
    val status_message: String
)
*/