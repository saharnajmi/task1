package com.example.task1.data.repository.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.task1.data.model.Generes

@Dao
interface GenreLocalDataSourceImpl : GenreLocalDataSource {

    @Query("SELECT * FROM genre")
    override fun getGenres(): List<Generes.Genre>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun saveGenres(genres: List<Generes.Genre>)

}