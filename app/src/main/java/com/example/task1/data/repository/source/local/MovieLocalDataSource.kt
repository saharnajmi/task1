package com.example.task1.data.repository.source.local

import com.example.task1.data.model.Movie

interface MovieLocalDataSource {
    fun getMovies(): List<Movie.Result>

    fun saveMovies(movies: List<Movie.Result>)

    fun deleteMovies()
}