package com.example.task1.data.repository.source

import com.example.task1.data.model.RequestToken
import com.example.task1.data.model.UserData

interface UserRepository {
    fun getRequestToken(): RequestToken

    fun validateWithLogin(
        username: String,
        password: String,
        requestToken: String
    ): RequestToken

    fun saveLogin(login: Boolean)

    fun saveUserData(userData: UserData)

    fun getUserData(): UserData

    fun checkLogin(): Boolean

    fun logOut()
}