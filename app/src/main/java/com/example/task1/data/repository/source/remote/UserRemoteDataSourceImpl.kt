package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.RequestToken
import com.example.task1.network.service.AuthService
import retrofit2.Call
import javax.inject.Inject

class UserRemoteDataSourceImpl @Inject constructor(private val authService: AuthService) :
    UserRemoteDataSource {

    override fun getRequestToken(): Call<RequestToken> =
        authService.getRequestToken()

    override fun validateWithLogin(
        username: String,
        password: String,
        requestToken: String
    ): Call<RequestToken> =
        authService.validateWithLogin(username, password, requestToken)
}