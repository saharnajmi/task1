package com.example.task1.data.repository.source.local

import com.example.task1.data.model.UserData

interface UserLocalDataSource {
    fun saveLogin(login: Boolean)

    fun saveUserData(userData: UserData)

    fun getUserData(): UserData

    fun checkLogin(): Boolean

    fun logOut()
}