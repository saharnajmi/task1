package com.example.task1.data.repository.source.local

import android.content.SharedPreferences
import com.example.task1.data.model.UserData
import javax.inject.Inject

class UserLocalDataSourceImpl @Inject constructor(private val sharedPreferences: SharedPreferences) :
    UserLocalDataSource {

    override fun saveLogin(login: Boolean) =
        sharedPreferences.edit().putBoolean("LOGIN", login).apply()

    override fun saveUserData(userData: UserData) {
        sharedPreferences.edit().apply {
            putString("USER_NAME", userData.userName)
            putString("PASSWORD", userData.password)
            putString("REQUEST_TOKEN", userData.requestToken)
        }.apply()
    }

    override fun getUserData(): UserData =
        UserData(
            sharedPreferences.getString("USER_NAME", "")!!,
            sharedPreferences.getString("PASSWORD", "")!!,
            sharedPreferences.getString("REQUEST_TOKEN", "")!!
        )

    override fun checkLogin() = sharedPreferences.getBoolean("LOGIN", false)

    override fun logOut() = sharedPreferences.edit().clear().apply()

}