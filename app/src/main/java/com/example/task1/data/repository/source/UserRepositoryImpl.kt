package com.example.task1.data.repository.source

import com.example.task1.common.AppError
import com.example.task1.data.model.RequestToken
import com.example.task1.data.model.UserData
import com.example.task1.data.repository.source.local.UserLocalDataSource
import com.example.task1.data.repository.source.remote.UserRemoteDataSource
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userRemoteDataSource: UserRemoteDataSource,
    private val userLocalDataSource: UserLocalDataSource
) : UserRepository {

    override fun getRequestToken(): RequestToken {
        val execute = userRemoteDataSource.getRequestToken().execute()

        if (execute.code() == 401)
            throw AppError(
                "Invalid request token",
                401,
                "توکن  نامعتبر است!"
            )

        if (!execute.isSuccessful) {
            throw AppError(
                "Error Network",
                401,
                "خطای شبکه"
            )
        }
        return execute.body()!!
    }

    override fun validateWithLogin(
        username: String,
        password: String,
        requestToken: String
    ): RequestToken {
        val execute = userRemoteDataSource.validateWithLogin(
            username,
            password,
            requestToken
        ).execute()
        if (!execute.isSuccessful) {
            if (execute.code() == 401) {
                throw AppError(
                    "Error Network",
                    401,
                    "خطای شبکه"
                )
            } else {
                throw AppError(
                    "Invalid username and/or password",
                    401,
                    "رمز عبور یا نام کاربری اشتباه است!"
                )
            }
        }
        return execute.body()!!
    }

    override fun saveLogin(login: Boolean) =
        userLocalDataSource.saveLogin(login)

    override fun saveUserData(userData: UserData) =
        userLocalDataSource.saveUserData(userData)

    override fun getUserData(): UserData =
        userLocalDataSource.getUserData()

    override fun checkLogin(): Boolean =
        userLocalDataSource.checkLogin()

    override fun logOut() =
        userLocalDataSource.logOut()
}