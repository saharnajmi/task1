package com.example.task1.data.repository.source.remote

import com.example.task1.data.model.Movie
import retrofit2.Call

interface MovieRemoteDataSource {
    fun getMovies(): Call<Movie>
}