package com.example.task1.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Generes(
    val genres: List<Genre>
) {
    @Entity(tableName = "genre")
    data class Genre(
        @PrimaryKey(autoGenerate = false)
        val id: Int,
        val name: String
    )
}