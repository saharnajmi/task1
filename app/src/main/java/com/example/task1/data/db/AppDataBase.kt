package com.example.task1.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.task1.data.model.Generes
import com.example.task1.data.model.IntTypeConverter
import com.example.task1.data.model.Movie
import com.example.task1.data.repository.source.local.GenreLocalDataSourceImpl
import com.example.task1.data.repository.source.local.MovieLocalDataSourceImpl

@Database(entities = [Movie.Result::class, Generes.Genre::class], version = 1)
@TypeConverters(IntTypeConverter::class)
abstract class AppDataBase : RoomDatabase() {
    abstract fun movieDao(): MovieLocalDataSourceImpl
    abstract fun genreDao(): GenreLocalDataSourceImpl
}